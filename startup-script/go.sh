#!bash
cd 
#Install basic things
#xcode-select --install
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install wget
brew install caskroom/cask/brew-cask
brew cask install xquartz
brew install wine
brew cask install sublime-text
brew cask install macs-fan-control
brew cask install unrarx
brew install python3
brew cask install arduino
brew cask install unity
#brew cask install the-unarchiver
#brew cask install vlc
#brew cask install virtualbox
#brew cask install google-chrome
#brew cask install google-drive
#brew cask install docker
#brew cask install docker-toolbox
#brew cask install filezilla
#brew cask install audio-hijack
#brew cask install vnc-viewer
#brew cask install vuze
#brew cask install vagrant
#brew cask install xmind
#brew cask install thunderbird
